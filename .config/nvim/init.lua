-- Initial plugin manager setup
require('jinja.lazy')

-- Sets, remaps, plugin-specific lua scripts
require('jinja.set')
require('jinja.remap')
require('jinja.devicons')
require('jinja.fugitive')
require('jinja.lsp')
require('jinja.telescope')
require('jinja.treesitter')
require('jinja.undotree')
