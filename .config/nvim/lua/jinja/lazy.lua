-- Lazy bootstrapping
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)
vim.g.mapleader = " "

-- Lazy setup
-- One-line plugins firt, then plugins with extra conf after
require('lazy').setup({
    'theprimeagen/harpoon',
    'mbbill/undotree',
    'preservim/nerdtree',
    'tpope/vim-fugitive',
    'tpope/vim-surround',
    'nvim-tree/nvim-web-devicons',
    'folke/trouble.nvim',
    'tomtom/tcomment_vim',
    'fatih/vim-go',
    -- Load colorscheme with high priority
    -- and set colors, background, etc when loaded
    {
        'morhetz/gruvbox',
        lazy = false,
        priority = 1000,
        config = function()
            vim.o.background = "dark"
            vim.cmd([[colorscheme gruvbox]])
            vim.cmd([[highlight! link SignColumn LineNr]])
        end,
    },
    {
        'nvim-telescope/telescope.nvim', tag = '0.1.5',
        dependencies = {'nvim-lua/plenary.nvim'}
    },
    {
        'nvim-telescope/telescope-file-browser.nvim',
        dependencies = {'nvim-lua/plenary.nvim', 'nvim-telescope/telescope.nvim'}
    },

    {
        'nvim-treesitter/nvim-treesitter', build = ':TSUpdate'
    },

    {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v3.x',
        dependencies = {
            --- Uncomment these if you want to manage LSP servers from neovim
            {'williamboman/mason.nvim'},
            {'williamboman/mason-lspconfig.nvim'},

            -- LSP Support
            {'neovim/nvim-lspconfig'},
            -- Autocompletion
            {'hrsh7th/nvim-cmp'},
            {'hrsh7th/cmp-nvim-lsp'},
            {'L3MON4D3/LuaSnip'},
        }
    },
})

