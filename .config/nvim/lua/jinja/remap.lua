-- File explorer
vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)
vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
vim.keymap.set({"n", "v"}, "<leader>/", vim.cmd.TComment)

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-d", "<C-d>zz")
vim.keymap.set("n", "<C-u", "<C-u>zz")

vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

vim.keymap.set("n", "<C-k>", "<cmd>cnext<CR>zz")
vim.keymap.set("n", "<C-j>", "<cmd>cprev<CR>zz")
vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

-- Copy, paste, delete keybinds for awesomeness
vim.keymap.set("n", "<leader>d", "\"_d")
vim.keymap.set("n", "<leader>d", "\"_d")
vim.keymap.set("n", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>p", "\"+p")

-- Tcomment maps
vim.keymap.set("n", "<leader>/", vim.cmd.TComment)
vim.keymap.set("v", "<leader>/", vim.cmd.TComment)

-- Telescope maps
vim.keymap.set("n", "<leader>gg", "<cmd>Telescope buffers<CR>")
vim.keymap.set("n", "<leader>ff", "<cmd>Telescope find_files<CR>")
vim.keymap.set("n", "<leader>fd", "<cmd>Telescope file_browser path=%:p:h<CR>")
vim.keymap.set("n", "<leader>fa", "<cmd>Telescope find_files hidden=true<CR>")
vim.keymap.set("n", "<leader>ft", "<cmd>Telescope treesitter<CR>")
vim.keymap.set("n", "<C-p>", "<cmd>Telescope git_files cwd=%:p:h<CR>")

vim.keymap.set("n", "<leader>tt", vim.cmd.NERDTree)

-- Golang maps
vim.keymap.set("n", "<leader>gf", vim.cmd.GoFmt)
vim.keymap.set("n", "<leader>gi", vim.cmd.GoImports)

-- Harpoon remaps
local mark = require("harpoon.mark")
local ui = require("harpoon.ui")

vim.keymap.set('n', '<leader>a', mark.add_file)
vim.keymap.set('n', '<C-e>', ui.toggle_quick_menu)

vim.keymap.set('n', '<C-j>', function() ui.nav_file(1) end)
vim.keymap.set('n', '<C-k>', function() ui.nav_file(2) end)
vim.keymap.set('n', '<C-l>', function() ui.nav_file(3) end)
vim.keymap.set('n', '<C-;>', function() ui.nav_file(4) end)

-- Trouble remaps
vim.keymap.set("n", "<leader>e", function() require("trouble").toggle() end)

