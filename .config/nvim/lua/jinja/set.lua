vim.opt.nu = true
vim.opt.relativenumber = true

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.smartindent = true

vim.opt.swapfile = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

vim.opt.hlsearch = false
vim.opt.ignorecase = true
vim.opt.incsearch = true

vim.opt.termguicolors = true
vim.opt.scrolloff = 5
vim.opt.sidescrolloff = 4
vim.opt.signcolumn = "auto"

vim.opt.backspace = "indent,eol,start"
vim.opt.wildmode = "longest,list,full"
vim.opt.wildmenu = true
vim.opt.mouse = "nvi"
