if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

XDG_CONFIG_HOME="$HOME/.config"

case "$OSTYPE" in 
    darwin*)
        host=$(hostname)
        if [ ${#host} -gt 25 ]; then
            export ZSH="/Users/wgraham/.oh-my-zsh"
        else
            export ZSH="/Users/will/.oh-my-zsh"
        fi
    ;;
    linux*)
        export ZSH="/home/will/.oh-my-zsh"
    ;;
esac

export PATH=$PATH:$(go env | grep PATH | awk 'BEGIN { FS = "\"" } ; {print $2}')/bin:/opt/nvim-linux64/bin:~/.local/bin
export EDITOR=nvim
fpath=($fpath ~/.zsh/completion)

ZSH_THEME="powerlevel10k/powerlevel10k"

# Auto-update frequency
zstyle ':omz:update' frequency 13

# History settings
export HISTFILE=~/.zsh_history
export HISTSIZE=1000000000
export SAVEHIST=$HISTSIZE
export HISTTIMEFORMAT="[%F %T] "
setopt INC_APPEND_HISTORY # append to history immediately
setopt EXTENDED_HISTORY # timestamps
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt SHARE_HISTORY

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

# ZSH Plugins
plugins=(
    docker
    docker-compose
    terraform
    kubectl
    git
    zsh-autosuggestions
    zsh-syntax-highlighting
)

# Global user settings
source $ZSH/oh-my-zsh.sh

bindkey -v
bindkey '^R' history-incremental-search-backward

alias cd='z'
alias cdi='zi'
<<<<<<< HEAD
=======
alias fd='fdfind'
>>>>>>> ee2822d7c700ce6501f2d3097b79a88f042fe12c
alias ls='ls -alhG'
alias gp='git push -u origin $(git rev-parse --abbrev-ref HEAD)'
alias cl=clear
alias py='python3'
alias vim='nvim'
alias nv='nvim'
alias v='nvim'
alias beer='echo "8J+Nugo=" | base64 -d'
alias melt='echo "8J+roAo=" | base64 -d'

function git_clean() {
    git branch --merged | grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn,.idea,.tox} -E -v "(^\*|master|main|dev|prod|lab)" | xargs git branch -d
    git prune origin
    git remote prune origin
    git gc
}

# This only works on fzf > 0.48. I have 0.42
# source <(fzf --zsh)
eval "$(zoxide init zsh)"

# To customize prompt, run 'p10k configure' or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

autoload -U +X bashcompinit && bashcompinit
complete -C '/usr/local/bin/aws_completer' aws

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/will/Downloads/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/will/Downloads/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/will/Downloads/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/will/Downloads/google-cloud-sdk/completion.zsh.inc'; fi
