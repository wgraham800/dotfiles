#!/usr/bin/bash
export HOME=$(pwd)

# Start with basic tools
apt update
apt install -y zsh curl zoxide golang gopls vim ripgrep fd-find fzf git yq gron

chsh -s $(which zsh)
# make sure to 'actually log out' and log back in after changing shells

# Install oh-my-zsh and some plugins
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
mkdir -p $HOME/workspace/ && cd $HOME/workspace
git clone https://gitlab.com/wgraham800/dotfiles

git clone https://github.com/zsh-users/zsh-autosuggestions $HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $HOME/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

cd $HOME/Downloads
# Neovim
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz
rm -rf /opt/nvim
tar -C /opt -xzf nvim-linux64.tar.gz

# fonts - pretty sure we can just copy into $HOME/.local/share/fonts and that should do it
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf

mkdir -p $HOME/.local/share/fonts
mv Meslo* $HOME/.local/share/fonts

wget https://github.com/Gogh-Co/Gogh/raw/master/apply-colors.sh
wget https://github.com/Gogh-Co/Gogh/raw/master/installs/gruvbox-material.sh
TERMINAL=gnome-terminal bash ./gruvbox-material.sh

# After getting theme and fonts, may have to change in gnome terminal preferences still

# Remove defaults and link the things
rm -rf $HOME/.zshrc $HOME/.tmux.conf $HOME/.gitconfig $HOME/.p10k.zsh $HOME/.config/nvim/*
mkdir -p $HOME/.config/nvim/
ln -s $HOME/workspace/dotfiles/.zshrc $HOME/.zshrc
ln -s $HOME/workspace/dotfiles/.tmux.conf $HOME/.tmux.conf
ln -s $HOME/workspace/dotfiles/.gitconfig $HOME/.gitconfig
ln -s $HOME/workspace/dotfiles/.p10k.zsh $HOME/.p10k.zsh
ln -s $HOME/workspace/dotfiles/.config/nvim/init.lua $HOME/.config/nvim/init.lua
ln -s $HOME/workspace/dotfiles/.config/nvim/lua $HOME/.config/nvim/lua

## TODO - add these things
# brave
# docker
# terraform
# awscli
